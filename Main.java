import zulu.Element;
import zulu.Field;
import zulu.Key;
import zulu.command.GetLayerBounds;
import zulu.command.LayerIntersectByBox;
import zulu.command.UpdateElemAttributes;
import zulu.response.ZuluHttpResponse;
import zulu.service.ZuluProxy;
import zulu.service.impl.ZuluProxyImpl;
import zulu.xml.ZuluRequest;
import org.json.JSONObject;
import org.json.XML;
import zulu.zws.BoundingBox;

import java.io.UnsupportedEncodingException;
import java.util.UUID;

import static zulu.enums.MapProjection.EPSG_4326;

/**
 * Created by santoninov on 20.11.17.
 */
public class Main {
//    ATM:Центральный район
//    ATM:DZR-test

//                "miny": 44.347494,
//                        "CRS": "EPSG:4326",
//                        "minx": 48.710027,
//                        "maxy": "44.521630",
//                        "maxx": 48.798576

    public static final String CUR_NAME = "ATM:DZR-test";
    public static void main(String... args) throws UnsupportedEncodingException {

        ZuluProxy zuluProxy = new ZuluProxyImpl();
        ZuluProxy zuluProxy1 = new ZuluProxyImpl();

        GetLayerBounds getLayerBounds = new GetLayerBounds();
        getLayerBounds.setLayer(CUR_NAME);


        ZuluRequest zuluRequest = new ZuluRequest(getLayerBounds);
        ZuluHttpResponse zuluHttpResponse = zuluProxy.makeSimpleRequest(zuluRequest);

        System.out.println(zuluHttpResponse.getStatusCode());
        if(zuluHttpResponse.getStatusCode() == 200){
            String str = new String(zuluHttpResponse.getByteArray(), "UTF-8");
            JSONObject xmlJSONObj = XML.toJSONObject(str);
            String jsonPrettyPrintString = xmlJSONObj.toString(4);
            System.out.println(jsonPrettyPrintString);

            JSONObject temp = xmlJSONObj.getJSONObject("zwsResponse")
                    .getJSONObject("GetLayerBounds")
                    .getJSONObject("Bounds")
                    .getJSONArray("BoundingBox")
                    .getJSONObject(0);

            Double minx = temp.getDouble("minx");
            Double miny = temp.getDouble("miny");
            Double maxx = temp.getDouble("maxx");
            Double maxy = temp.getDouble("maxy");

            LayerIntersectByBox layerIntersectByBox = new LayerIntersectByBox();
            BoundingBox boundingBox = new BoundingBox();
            boundingBox.setCrs(EPSG_4326);
            boundingBox.setMinx(minx);
            boundingBox.setMiny(miny);
            boundingBox.setMaxx(maxx);
            boundingBox.setMaxy(maxy);

            layerIntersectByBox.setLayer(CUR_NAME);
            layerIntersectByBox.setBoundingBox(boundingBox);
            layerIntersectByBox.setGeometry(false);
            layerIntersectByBox.setAttr(false);
            layerIntersectByBox.setModelList(false);


            ZuluRequest zuluRequest1 = new ZuluRequest(layerIntersectByBox);
            ZuluHttpResponse zuluHttpResponse1 = zuluProxy1.makeSimpleRequest(zuluRequest1);

            System.out.println(zuluHttpResponse1.getStatusCode());
            if(zuluHttpResponse1.getStatusCode() == 200){

                str = new String(zuluHttpResponse1.getByteArray(), "UTF-8");
                xmlJSONObj = XML.toJSONObject(str);
                jsonPrettyPrintString = xmlJSONObj.toString(4);
//                System.out.println(jsonPrettyPrintString);

                Integer length = xmlJSONObj.getJSONObject("zwsResponse").getJSONObject("LayerIntersectByBox").getJSONArray("Element").length();
                Integer sys;
                for (int i = 0; i < length; i++) {
                    sys = xmlJSONObj.getJSONObject("zwsResponse").getJSONObject("LayerIntersectByBox").getJSONArray("Element").getJSONObject(i).getInt("ElemID");
                    UpdateElemAttributes updateElemAttributes = new UpdateElemAttributes();

                    Key key = new Key();
                    key.setName("Sys");
                    key.setValue(sys);

                    Field value = new Field();
                    value.setName("guid");
                    value.setValue(UUID.randomUUID().toString());

                    Element element = new Element();

                    element.setKey(key);
                    element.setField(value);

                    updateElemAttributes.setLayer("ATM:DZR-test");
                    updateElemAttributes.setElement(element);

                    zuluRequest = new ZuluRequest(updateElemAttributes);
                    zuluHttpResponse = zuluProxy.makeSimpleRequest(zuluRequest);
                    System.out.println("ststuscode = " + zuluHttpResponse.getStatusCode() + " " + "sys = " + sys + " " + "index = " + i);
                }

            }

        }

    }
}
