package zulu.zws.response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Created by silwer on 11.10.17.
 */
@XmlTransient
public class AbstractZwsResponse {

    private Integer retVal;

    @XmlElement(name = "RetVal")
    public Integer getRetVal() {
        return retVal;
    }

    public void setRetVal(Integer retVal) {
        this.retVal = retVal;
    }
}
