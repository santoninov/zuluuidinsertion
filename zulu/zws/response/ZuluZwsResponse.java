package zulu.zws.response;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by silwer on 11.10.17.
 */
@XmlRootElement(name = "zwsResponse")
public class ZuluZwsResponse {

    private Object response;
    private Integer retVal;


    @XmlElements(value = {
            @XmlElement(name = "GetLayerList", type = GetLayerList.class)
    })
    public Object getResponse() {
        return response;
    }

    public void setResponse(Object response) {
        this.response = response;
    }

    @XmlElement(name = "RetVal")
    public Integer getRetVal() {
        return retVal;
    }

    public void setRetVal(Integer retVal) {
        this.retVal = retVal;
    }
}
