package zulu.zws.response;


import zulu.zws.types.LayerType;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;

/**
 * Created by silwer on 11.10.17.
 */
@XmlType
public class GetLayerList {

    private ArrayList<LayerType> layers;

    public GetLayerList() {
        layers = new ArrayList<>();
    }

    public GetLayerList(ArrayList<LayerType> layers) {
        this.layers = layers;
    }

    @XmlElement(name = "Layer")
    public ArrayList<LayerType> getLayers() {
        return layers;
    }

    public void setLayers(ArrayList<LayerType> layers) {
        this.layers = layers;
    }
}
