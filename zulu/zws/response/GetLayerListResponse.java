package zulu.zws.response;


import zulu.zws.types.LayerType;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;

/**
 * Created by silwer on 11.10.17.
 */
@XmlRootElement(name = "zwsResponse")
@XmlType(propOrder = {"layerList", "retVal"})
public class GetLayerListResponse extends AbstractZwsResponse {

    private ArrayList<LayerType> layerList = new ArrayList<>();


    @XmlElementWrapper(name = "GetLayerList")
    @XmlElement(name = "Layer")
    public ArrayList<LayerType> getLayerList() {
        return layerList;
    }

    public void setLayerList(ArrayList<LayerType> layerList) {
        this.layerList = layerList;
    }
}
