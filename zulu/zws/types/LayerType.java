package zulu.zws.types;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by silwer on 11.10.17.
 */
@XmlType(name = "Layer")
public class LayerType {

    private String name;
    private String title;


    public LayerType() {
    }

    public LayerType(String name, String title) {
        this.name = name;
        this.title = title;
    }

    @XmlElement(name = "Name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlElement(name = "Title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
