package zulu.zws;

import zulu.enums.MapProjection;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by santoninov on 24.10.17.
 */
@XmlType(name = "BoundingBox", propOrder = {"crs", "minx", "miny", "maxx", "maxy"})
public class BoundingBox {

    private MapProjection crs;
    private Double minx;
    private Double miny;
    private Double maxx;
    private Double maxy;

    @XmlElement(name = "CRS")
    public MapProjection getCrs() {
        return crs;
    }

    @XmlElement(name = "minx")
    public Double getMinx() {
        return minx;
    }

    @XmlElement(name = "miny")
    public Double getMiny() {
        return miny;
    }

    @XmlElement(name = "maxx")
    public Double getMaxx() {
        return maxx;
    }

    @XmlElement(name = "maxy")
    public Double getMaxy() {
        return maxy;
    }

    public void setMaxy(Double maxy) {
        this.maxy = maxy;
    }
    public void setMaxx(Double maxx) {
        this.maxx = maxx;
    }
    public void setMiny(Double miny) {
        this.miny = miny;
    }
    public void setMinx(Double minx) {
        this.minx = minx;
    }
    public void setCrs(MapProjection crs) {
        this.crs = crs;
    }
}

