package zulu.service;


import zulu.response.ZuluHttpResponse;
import zulu.xml.ZuluRequest;

import javax.xml.bind.JAXBException;

/**
 * Created by silwer on 09.10.17.
 */
public interface ZuluProxy {

    /**
     * Подготовка запроса (строка xml)
     * */
    public String prepareXmlRequestString(ZuluRequest zuluRequest) throws JAXBException;
    public String prepareXmlRequestString(ZuluRequest zuluRequest, Boolean formatted) throws JAXBException;

    /**
     * Выполнение простого запроса на сервер Zulu без логической обработки ответа
     * */
    ZuluHttpResponse makeSimpleRequest(ZuluRequest zuluRequest);
}
