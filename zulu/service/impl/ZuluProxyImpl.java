package zulu.service.impl;

import org.apache.http.HttpEntity;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import zulu.response.HttpClientErrorReason;
import zulu.response.ZuluHttpResponse;
import zulu.service.ZuluProxy;
import zulu.xml.ZuluRequest;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.IOException;
import java.io.StringWriter;

/**
 * Created by silwer on 09.10.17.
 */
public class ZuluProxyImpl implements ZuluProxy {

    private String ZULU_ZWS_URL = "https://model.teplovolgograd.ru/zws";
//    private String ZULU_USER = "zulu-uss";
    private String ZULU_USER = "s.antoninov";
//    private String ZULU_PASSWORD = "&5X%L[L)](";
    private String ZULU_PASSWORD = "santoninovzulu";

    /**
     * Подготовка запроса (строка xml)
     * */
    public String prepareXmlRequestString(ZuluRequest zuluRequest) throws JAXBException {
        return  prepareXmlRequestString(zuluRequest, false);
    }


    public String prepareXmlRequestString(ZuluRequest zuluRequest, Boolean formatted) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(ZuluRequest.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, formatted);

        StringWriter stringWriter = new StringWriter();

        jaxbMarshaller.marshal(zuluRequest, stringWriter);

        return stringWriter.toString();
    }

    /**
     * HttpClient
     * */
    private ZuluHttpResponse makeRequest(ZuluRequest zuluRequest) throws AuthenticationException, JAXBException, IOException {
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(ZULU_ZWS_URL);

        UsernamePasswordCredentials basicAuth = new UsernamePasswordCredentials(ZULU_USER, ZULU_PASSWORD);
        httpPost.addHeader(new BasicScheme().authenticate(basicAuth, httpPost, null));

        HttpEntity xmlRequestEntity = new ByteArrayEntity(prepareXmlRequestString(zuluRequest).getBytes("UTF-8"));
        httpPost.setEntity(xmlRequestEntity);
        CloseableHttpResponse response = client.execute(httpPost);

        ZuluHttpResponse zuluHttpResponse = new ZuluHttpResponse();

        zuluHttpResponse.setStatusCode(response.getStatusLine().getStatusCode());
        zuluHttpResponse.setHttpEntity(response.getEntity());
        zuluHttpResponse.setByteArray(EntityUtils.toByteArray(response.getEntity()));

        client.close();

        return zuluHttpResponse;
    }

    /**
     * Обертка метода с простой обработкой эксепшинов
     * */
    private ZuluHttpResponse makeRequestWrapper(ZuluRequest zuluRequest) {
        try {
            return makeRequest(zuluRequest);
        } catch (AuthenticationException e) {
            e.printStackTrace();
            return new ZuluHttpResponse(400, null, HttpClientErrorReason.AUTHENTICATION_EXCEPTION);
        } catch (JAXBException e) {
            e.printStackTrace();
            return new ZuluHttpResponse(400, null, HttpClientErrorReason.JAXB_EXCEPTION);
        } catch (IOException e) {
            e.printStackTrace();
            return new ZuluHttpResponse(400, null, HttpClientErrorReason.IO_EXCEPTION);
        }
    }

    /**
     * Выполнение простого запроса на сервер Zulu без логической обработки ответа
     *
     * @param zuluRequest
     */

    public ZuluHttpResponse makeSimpleRequest(ZuluRequest zuluRequest) {
        return makeRequestWrapper(zuluRequest);
    }
}
