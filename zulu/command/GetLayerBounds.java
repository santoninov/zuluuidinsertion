package zulu.command;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by santoninov on 20.11.17.
 */
@XmlType(name = "GetLayerBounds")
public class GetLayerBounds {

    private String layer;

    public GetLayerBounds() {
    }

    public GetLayerBounds(String layer) {
        this.layer = layer;
    }

    @XmlElement(name = "Layer")
    public String getLayer() {
        return layer;
    }

    public void setLayer(String layer) {
        this.layer = layer;
    }
}
