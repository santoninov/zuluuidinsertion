package zulu.command;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by santoninov on 16.11.17.
 */
@XmlType(name = "GetLayerLabels")
public class GetLayerLabels {

    private String layer;

    public GetLayerLabels() {
    }

    public GetLayerLabels(String layer) {
        this.layer = layer;
    }

    @XmlElement(name = "Layer")
    public String getLayer() {
        return layer;
    }

    public void setLayer(String layer) {
        this.layer = layer;
    }
}
