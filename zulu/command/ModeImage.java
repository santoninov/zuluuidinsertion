package zulu.command;


import zulu.enums.ModeImageFormat;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by silwer on 10.10.17.
 */
@XmlType(name = "ModeImage", propOrder = {"width", "height", "format"})
public class ModeImage {

    private Integer width = 72;
    private Integer height = 72;
    private ModeImageFormat format = ModeImageFormat.DATA_URI;


    public ModeImage() {
    }

    public ModeImage(Integer width, Integer height) {
        this.width = width;
        this.height = height;
    }

    public ModeImage(Integer width, Integer height, ModeImageFormat format) {
        this.width = width;
        this.height = height;
        this.format = format;
    }


    @XmlElement(name = "Width")
    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    @XmlElement(name = "Height")
    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    @XmlElement(name = "Format")
    public ModeImageFormat getFormat() {
        return format;
    }

    public void setFormat(ModeImageFormat format) {
        this.format = format;
    }
}
