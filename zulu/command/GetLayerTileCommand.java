package zulu.command;

import zulu.adapters.BooleanAdapter;
import zulu.adapters.ListOfIntegersAdapter;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by silwer on 09.10.17.
 */
@XmlType(name = "GetLayerTile", propOrder = {"x", "y", "z", "layer", "showDirection", "labels"})
public class GetLayerTileCommand {

    private Integer x;
    private Integer y;
    private Integer z;
    private String layer;
    private Boolean showDirection = true;
    private List<Integer> labels = new ArrayList<>();

    public GetLayerTileCommand() {
    }

    public GetLayerTileCommand(Integer x, Integer y, Integer z, String layer) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.layer = layer;
    }


    @XmlElement
    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    @XmlElement
    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

    @XmlElement
    public Integer getZ() {
        return z;
    }

    public void setZ(Integer z) {
        this.z = z;
    }

    @XmlElement(name = "Layer")
    public String getLayer() {
        return layer;
    }

    public void setLayer(String layer) {
        this.layer = layer;
    }

    @XmlElement(name = "ShowDirection")
    @XmlJavaTypeAdapter(BooleanAdapter.class)
    public Boolean getShowDirection() {
        return showDirection;
    }

    public void setShowDirection(Boolean showDirection) {
        this.showDirection = showDirection;
    }

    @XmlElement(name = "Labels")
    @XmlJavaTypeAdapter(ListOfIntegersAdapter.class)
    public List<Integer> getLabels() {
        return labels;
    }

    public void setLabels(List<Integer> labels) {
        this.labels = labels;
    }
}
