package zulu.command;

import zulu.Element;
import zulu.Field;
import zulu.Key;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by santoninov on 20.11.17.
 */
@XmlType(name = "UpdateElemAttributes")
public class UpdateElemAttributes {

    private String layer;

    private Element element;

    public UpdateElemAttributes () {
    }

    public UpdateElemAttributes (String layer) {
        this.layer = layer;
    }

    @XmlElement(name = "Layer")
    public String getLayer() {
        return layer;
    }

    public void setLayer(String layer) {
        this.layer = layer;
    }

    @XmlElement(name = "Element")
    public Element getElement() {
        return element;
    }

    public void setElement(Element element) {
        this.element = element;
    }
}
