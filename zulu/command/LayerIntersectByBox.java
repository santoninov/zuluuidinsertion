package zulu.command;


import zulu.adapters.BooleanAdapter;
import zulu.zws.BoundingBox;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * Created by santoninov on 24.10.17.
 */
@XmlType(name = "", propOrder = {"layer", "boundingBox", "geometry", "attr", "modeList"})
public class LayerIntersectByBox {

    private String layer;

    private BoundingBox boundingBox;

    private Boolean geometry;

    private Boolean attr;
    private Boolean modeList;

    public LayerIntersectByBox() {
    }


    @XmlElement(name = "Layer")
    public String getLayer() {
        return layer;
    }

    @XmlElement(name = "Geometry")
    @XmlJavaTypeAdapter(BooleanAdapter.class)
    public Boolean getGeometry() {
        return geometry;
    }

    @XmlElement(name = "Attr")
    @XmlJavaTypeAdapter(BooleanAdapter.class)
    public Boolean getAttr() {
        return attr;
    }


    public void setLayer(String layer) {
        this.layer = layer;
    }


    public void setGeometry(Boolean geometry) {
        this.geometry = geometry;
    }

    public void setAttr(Boolean attr) {
        this.attr = attr;
    }

    public void setModelList(Boolean modeList) {
        this.modeList = modeList;
    }

    @XmlElement(name = "ModeList")
    @XmlJavaTypeAdapter(BooleanAdapter.class)
    public Boolean getModeList() {
        return modeList;
    }

    public void setBoundingBox(BoundingBox boundingBox) {
        this.boundingBox = boundingBox;
    }

    @XmlElement(name = "BoundingBox")
    public BoundingBox getBoundingBox() {
        return boundingBox;
    }

}
