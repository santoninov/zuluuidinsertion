package zulu.command;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by silwer on 09.10.17.
 */
@XmlType(name = "CommandWrapper")
public class CommandWrapper {


    private Object command;

    @XmlElements(value = {
            @XmlElement(name = "GetLayerTile", type = GetLayerTileCommand.class),
            @XmlElement(name = "SelectElemByXY", type = SelectElemByXYCommand.class),
            @XmlElement(name = "GetLayerList", type = GetLayerListCommand.class),
            @XmlElement(name = "LayerIntersectByBox", type = LayerIntersectByBox.class),
            @XmlElement(name = "GetLayerLabels", type = GetLayerLabels.class),
            @XmlElement(name = "GetLayerBounds", type = GetLayerBounds.class),
            @XmlElement(name = "UpdateElemAttributes", type = UpdateElemAttributes.class)
    })
    public Object getCommand() {
        return command;
    }

    public void setCommand(Object command) {
        this.command = command;
    }
}
