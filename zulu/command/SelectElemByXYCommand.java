package zulu.command;


import zulu.adapters.BooleanAdapter;
import zulu.enums.MapProjection;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * Created by silwer on 10.10.17.
 */
@XmlType(name = "", propOrder = {"layer", "x", "y", "scale", "crs", "geometry", "geometryLinks", "attr", "modeList", "modeImage"})
public class SelectElemByXYCommand {

    private String layer;
    private Double x;
    private Double y;
    private Double scale;
    private MapProjection crs;
    private Boolean geometry;
    private MapProjection geometryLinks;
    private Boolean attr;
    private Boolean modeList;
    private ModeImage modeImage = new ModeImage();


    public SelectElemByXYCommand() {
    }


    @XmlElement(name = "Layer")
    public String getLayer() {
        return layer;
    }

    @XmlElement(name = "X")
    public Double getX() {
        return x;
    }

    @XmlElement(name = "Y")
    public Double getY() {
        return y;
    }

    @XmlElement(name = "Scale")
    public Double getScale() {
        return scale;
    }

    @XmlElement(name = "CRS")
    public MapProjection getCrs() {
        return crs;
    }

    @XmlElement(name = "Geometry")
    @XmlJavaTypeAdapter(BooleanAdapter.class)
    public Boolean getGeometry() {
        return geometry;
    }

    @XmlElement(name = "GeometryLinks")
    public MapProjection getGeometryLinks() {
        return geometryLinks;
    }

    @XmlElement(name = "Attr")
    @XmlJavaTypeAdapter(BooleanAdapter.class)
    public Boolean getAttr() {
        return attr;
    }

    @XmlElement(name = "ModeList")
    @XmlJavaTypeAdapter(BooleanAdapter.class)
    public Boolean getModeList() {
        return modeList;
    }

    @XmlElement(name = "ModeImage")
    public ModeImage getModeImage() {
        return modeImage;
    }

    public void setLayer(String layer) {
        this.layer = layer;
    }

    public void setX(Double x) {
        this.x = x;
    }

    public void setY(Double y) {
        this.y = y;
    }

    public void setScale(Double scale) {
        this.scale = scale;
    }

    public void setCrs(MapProjection crs) {
        this.crs = crs;
    }

    public void setGeometry(Boolean geometry) {
        this.geometry = geometry;
    }

    public void setGeometryLinks(MapProjection geometryLinks) {
        this.geometryLinks = geometryLinks;
    }

    public void setAttr(Boolean attr) {
        this.attr = attr;
    }

    public void setModelList(Boolean modeList) {
        this.modeList = modeList;
    }

    public void setModeImage(ModeImage modeImage) {
        this.modeImage = modeImage;
    }
}
