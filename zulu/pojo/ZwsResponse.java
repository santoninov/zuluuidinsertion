package zulu.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by santoninov on 18.10.17.
 */
public class ZwsResponse {

    private Integer RetVal;

    private SelectElemByXY SelectElemByXY;

    public Integer getRetVal() {
        return RetVal;
    }

    @JsonProperty("RetVal")
    public void setRetVal(Integer retVal) {
        RetVal = retVal;
    }

    public SelectElemByXY getSelectElemByXY() {
        return SelectElemByXY;
    }

    @JsonProperty("SelectElemByXY")
    public void setSelectElemByXY(SelectElemByXY selectElemByXY) {
//        if(selectElemByXY instanceof String){
//
//        }
        SelectElemByXY = selectElemByXY;
    }
}
