package zulu.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by santoninov on 18.10.17.
 */
public class Field
{
    private boolean ReadOnly;

    private String Type;

    private String UserName;

    private String Value;

    private boolean Key;

    private String Name;

    private Book book;

    public Book getBook() {
        return book;
    }

    @JsonProperty("Book")
    public void setBook(Book book) {
        this.book = book;
    }
    @JsonProperty("ReadOnly")
    public void setReadOnly(boolean ReadOnly){
        this.ReadOnly = ReadOnly;
    }
    public boolean getReadOnly(){
        return this.ReadOnly;
    }
    @JsonProperty("Type")
    public void setType(String Type){
        this.Type = Type;
    }
    public String getType(){
        return this.Type;
    }
    @JsonProperty("UserName")
    public void setUserName(String UserName){
        this.UserName = UserName;
    }
    public String getUserName(){
        return this.UserName;
    }
    @JsonProperty("Value")
    public void setValue(String Value){
        this.Value = Value;
    }
    public String getValue(){
        return this.Value;
    }
    @JsonProperty("Key")
    public void setKey(boolean Key){
        this.Key = Key;
    }
    public boolean getKey(){
        return this.Key;
    }
    @JsonProperty("Name")
    public void setName(String Name){
        this.Name = Name;
    }
    public String getName(){
        return this.Name;
    }
}
