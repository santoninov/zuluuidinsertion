package zulu.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by santoninov on 18.10.17.
 */
public class BookRecord
{
    @JsonProperty("Value")
    private String Value;

    @JsonProperty("Code")
    private int Code;

    public void setValue(String Value){
        this.Value = Value;
    }
    public String getValue(){
        return this.Value;
    }
    public void setCode(int Code){
        this.Code = Code;
    }
    public int getCode(){
        return this.Code;
    }
}
