package zulu.pojo;

/**
 * Created by santoninov on 18.10.17.
 */
public class Root {

    private ZwsResponse zwsResponse;

    public ZwsResponse getZwsResponse() {
        return zwsResponse;
    }

    public void setZwsResponse(ZwsResponse zwsResponse) {
        this.zwsResponse = zwsResponse;
    }
}
