package zulu.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by santoninov on 18.10.17.
 */
public class SelectElemByXY{

    private Element Element;

    public Element getElement() {
        return Element;
    }

    @JsonProperty("Element")
    public void setElement(Element element) {
        Element = element;
    }
}
