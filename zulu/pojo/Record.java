package zulu.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by santoninov on 18.10.17.
 */
public class Record
{

    private List<Field> Field;

    @JsonProperty("Field")
    public void setField(List<Field> Field){
        this.Field = Field;
    }
    public List<Field> getField(){
        return this.Field;
    }
}
