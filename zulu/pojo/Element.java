package zulu.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by santoninov on 18.10.17.
 */

public class Element
{
    private int TypeID;

    private Modes Modes;

    private int ElemID;

    private int ModeNum;

    private Records Records;

    @JsonProperty("TypeID")
    public void setTypeID(int TypeID){
        this.TypeID = TypeID;
    }
    public int getTypeID(){
        return this.TypeID;
    }
    @JsonProperty("Modes")
    public void setModes(Modes Modes){
        this.Modes = Modes;
    }
    public Modes getModes(){
        return this.Modes;
    }
    @JsonProperty("ElemID")
    public void setElemID(int ElemID){
        this.ElemID = ElemID;
    }
    public int getElemID(){
        return this.ElemID;
    }
    @JsonProperty("ModeNum")
    public void setModeNum(int ModeNum){
        this.ModeNum = ModeNum;
    }
    public int getModeNum(){
        return this.ModeNum;
    }
    @JsonProperty("Records")
    public void setRecords(Records Records){
        this.Records = Records;
    }
    public Records getRecords(){
        return this.Records;
    }
}


