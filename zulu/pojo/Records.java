package zulu.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by santoninov on 18.10.17.
 */
public class Records
{
    private int BaseID;

    private Record Record;

    private String QueryName;

    @JsonProperty("BaseID")
    public void setBaseID(int BaseID){
        this.BaseID = BaseID;
    }
    public int getBaseID(){
        return this.BaseID;
    }
    @JsonProperty("Record")
    public void setRecord(Record Record){
        this.Record = Record;
    }
    public Record getRecord(){
        return this.Record;
    }
    @JsonProperty("QueryName")
    public void setQueryName(String QueryName){
        this.QueryName = QueryName;
    }
    public String getQueryName(){
        return this.QueryName;
    }
}
