package zulu.pojo;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by santoninov on 18.10.17.
 */
public class Book
{
    @JsonProperty("Record")
    private List<BookRecord> Record;

    @JsonProperty("Name")
    private String Name;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public void setRecord(List<BookRecord> Record){
        this.Record = Record;
    }
    public List<BookRecord> getRecord(){
        return this.Record;
    }
}
