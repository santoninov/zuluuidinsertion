package zulu.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by santoninov on 18.10.17.
 */
public class Mode
{
    private String SwitchState;

    private String Title;

    private int Index;

    private String Image;

    @JsonProperty("SwitchState")
    public void setSwitchState(String SwitchState){
        this.SwitchState = SwitchState;
    }
    public String getSwitchState(){
        return this.SwitchState;
    }
    @JsonProperty("Title")
    public void setTitle(String Title){
        this.Title = Title;
    }
    public String getTitle(){
        return this.Title;
    }
    @JsonProperty("Index")
    public void setIndex(int Index){
        this.Index = Index;
    }
    public int getIndex(){
        return this.Index;
    }
    @JsonProperty("Image")
    public void setImage(String Image){
        this.Image = Image;
    }
    public String getImage(){
        return this.Image;
    }
}
