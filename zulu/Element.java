package zulu;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by santoninov on 21.11.17.
 */
@XmlType(name = "Element")
public class Element {

    private Key key;

    private Field field;

    @XmlElement(name = "Key")
    public Key getKey() {
        return key;
    }

    public void setKey(Key element) {
        this.key = element;
    }

    @XmlElement(name = "Field")
    public Field getField() {
        return field;
    }

    public void setField(Field element1) {
        this.field = element1;
    }
}
