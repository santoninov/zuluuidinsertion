package zulu.enums;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by silwer on 10.10.17.
 */
@XmlType(name = "CRS")
@XmlEnum
public enum MapProjection {

    @XmlEnumValue("EPSG:4326")
    EPSG_4326("EPSG:4326"),

    @XmlEnumValue("EPSG:3857")
    EPSG_3857("EPSG:3857");

    private final String value;


    MapProjection(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }

    public static MapProjection fromValue(String v) {
        for (MapProjection c: MapProjection.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
