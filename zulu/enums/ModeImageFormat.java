package zulu.enums;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * Created by silwer on 10.10.17.
 */
@XmlType(name = "Format")
@XmlEnum
public enum ModeImageFormat {

    @XmlEnumValue("DataURI")
    DATA_URI("DataURI");

    private final String value;


    ModeImageFormat(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }

    public static ModeImageFormat fromValue(String v) {
        for (ModeImageFormat c: ModeImageFormat.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }
}
