package zulu.response;

/**
 * Created by silwer on 09.10.17.
 */
public class HttpClientErrorReason {

    public static final String JAXB_EXCEPTION = "JAXBException";
    public static final String IO_EXCEPTION = "IOException";
    public static final String AUTHENTICATION_EXCEPTION = "AuthenticationException";

    private HttpClientErrorReason() {
    }
}
