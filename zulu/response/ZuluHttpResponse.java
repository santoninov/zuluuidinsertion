package zulu.response;

import org.apache.http.HttpEntity;

/**
 * Created by silwer on 09.10.17.
 */
public class ZuluHttpResponse {

    private int statusCode;
    private HttpEntity httpEntity;
    private String reason;
    private byte[] byteArray;


    public ZuluHttpResponse() {
    }

    public ZuluHttpResponse(int statusCode, HttpEntity httpEntity) {
        this.statusCode = statusCode;
        this.httpEntity = httpEntity;
    }

    public ZuluHttpResponse(int statusCode, HttpEntity httpEntity, String reason) {
        this.statusCode = statusCode;
        this.httpEntity = httpEntity;
        this.reason = reason;
    }


    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public HttpEntity getHttpEntity() {
        return httpEntity;
    }

    public void setHttpEntity(HttpEntity httpEntity) {
        this.httpEntity = httpEntity;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public byte[] getByteArray() {
        return byteArray;
    }

    public void setByteArray(byte[] byteArray) {
        this.byteArray = byteArray;
    }
}
