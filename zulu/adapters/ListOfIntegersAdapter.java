package zulu.adapters;

import javax.xml.bind.ValidationEventHandler;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by silwer on 17.10.17.
 */
public class ListOfIntegersAdapter extends XmlAdapter<String, List<Integer>> {
    /**
     * Convert a value type to a bound type.
     *
     * @param v The value to be converted. Can be null.
     * @throws Exception if there's an error during the conversion. The caller is responsible for
     *                   reporting the error to the user through {@link ValidationEventHandler}.
     */
    @Override
    public List<Integer> unmarshal(String v) throws Exception {
        String[] stringIntegers = v.split(" ");
        List<Integer> listOfIntegers = new ArrayList<>();

        if (stringIntegers.length > 0) {
            for (String stringInteger : stringIntegers) {
                listOfIntegers.add(Integer.valueOf(stringInteger));
            }
        }

        return listOfIntegers;
    }

    /**
     * Convert a bound type to a value type.
     *
     * @param v The value to be convereted. Can be null.
     * @throws Exception if there's an error during the conversion. The caller is responsible for
     *                   reporting the error to the user through {@link ValidationEventHandler}.
     */
    @Override
    public String marshal(List<Integer> v) throws Exception {

// здесь поменять пришлось. Че-то у меня ошибку выдавало с стримами.
        if (v.size() > 0) {
            String temp = "";
            for (int i = 0; i < v.size(); i++) {
                temp += v.get(i) + " ";
            }
            return temp;
        }

//        if (v.size() > 0) {
//            return v.stream().map(Object::toString)
//                    .collect(Collectors.joining(" "));
//        }
        return "";
    }
}
