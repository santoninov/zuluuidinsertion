package zulu.adapters;

import javax.xml.bind.ValidationEventHandler;
import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * Created by silwer on 10.10.17.
 */
public class BooleanAdapter extends XmlAdapter<String, Boolean> {
    /**
     * Convert a value type to a bound type.
     *
     * @param v The value to be converted. Can be null.
     * @throws Exception if there's an error during the conversion. The caller is responsible for
     *                   reporting the error to the user through {@link ValidationEventHandler}.
     */
    @Override
    public Boolean unmarshal(String v) throws Exception {
        return "yes".equals(v.toLowerCase());
    }

    /**
     * Convert a bound type to a value type.
     *
     * @param v The value to be convereted. Can be null.
     * @throws Exception if there's an error during the conversion. The caller is responsible for
     *                   reporting the error to the user through {@link ValidationEventHandler}.
     */
    @Override
    public String marshal(Boolean v) throws Exception {
        if (v) {
            return "Yes";
        }
        return "No";
    }
}
