package zulu.xml;


import zulu.command.CommandWrapper;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by silwer on 09.10.17.
 */
@XmlRootElement(name = "zulu-server")
public class ZuluRequest {

    private String service = "zws";
    private String version = "1.0.0";

    private CommandWrapper commandWrapper = new CommandWrapper();


    public ZuluRequest() {
    }

    public ZuluRequest(Object command) {
        commandWrapper.setCommand(command);
    }


    @XmlAttribute
    public String getService() {
        return service;
    }

    @XmlAttribute
    public String getVersion() {
        return version;
    }

    @XmlElement(name = "Command")
    public CommandWrapper getCommandWrapper() {
        return commandWrapper;
    }

    public void setCommandWrapper(CommandWrapper commandWrapper) {
        this.commandWrapper = commandWrapper;
    }
}
